"""
Script is just sample to test cicd
"""
def is_even(number):
    """
    return True if even numbmer
    """
    if number % 2 == 0:
        return True
    return False

def test_even():
    """
    tests
    """
    assert  is_even(3) == False

def main():
    """
    main function
    """
    print("Hello")
    print(is_even(3))

if __name__ == "__main__":
    main()
